class CreatePhoneRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :phone_records do |t|
      t.integer :prefix
      t.integer :max_len
      t.integer :min_len
      t.string :usage
      t.string :comment

      t.timestamps
    end
  end
end
