
module PhoneRecordsRepository
    def find_by_number(prefixes)
        PhoneRecord.where(prefix: prefixes).order(prefix: :desc)
    end

    def find_by_name(names)
        find_by_name_relation = names.empty?? PhoneRecord.none : PhoneRecord.where(get_prepared_statement(names))
        find_by_name_relation.order(prefix: :desc)
    end

    private

    def get_prepared_statement(names)
        "phone_records.comment LIKE '#{names.join("%' OR phone_records.comment LIKE '")}%'"
    end
end