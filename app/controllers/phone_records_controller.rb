
class PhoneRecordsController < ApplicationController
  before_action :set_phone_records_collection, only: [:index]
  before_action :clean_query, only: [:index]

  # GET /phone_records?criteria=<search_criteria>&page=<page-number>&per_page=<records_per_page>
  def index
    @phone_records = params[:criteria].nil?? PhoneRecord.all : @phone_records_collection.find_by_criteria(params[:criteria])

    paginate json: @phone_records
  end

  private

    def set_phone_records_collection
      @phone_records_collection = PhoneRecords.new
    end

    def clean_query
      [' (0) ',' (0)','(0) ','(0)'].map { |zero| params[:criteria].gsub! zero, '' } unless params[:criteria].nil?
    end

end
