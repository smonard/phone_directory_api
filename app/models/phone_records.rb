
class PhoneRecords
    def find_by_criteria(criteria)
        name_relation = PhoneRecord.find_by_name(get_names(criteria))
        number_relation = PhoneRecord.find_by_number(get_prefixes(criteria))
        return number_relation .or name_relation 
    end

    private

    def get_prefixes(criteria)
        criteria.scan(/\d+/).map! { |str_num| get_divided_prefixes(str_num.to_i) }.flatten
    end

    def get_names(criteria)
        criteria.scan(/[a-zßäëïöüA-ZÄËÏÖÜ\\-]+/)
    end

    def get_divided_prefixes(number)
        prefixes = Array.new
        while (number > 0) 
            prefixes << number
            number /= 10;
        end
        prefixes
    end
end