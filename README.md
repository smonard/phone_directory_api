# Phone Directory API

API to serve searches over a phone prefixes directory.

Rails based API (--api)
  - Rails Version: 5.3
  - Ruby Version: 2.5

Useful commands:

* Install dependencies

        bundle install --path vendor/bundle

* Database creation

        bundle exec rake db:create

* Database initialization

        bundle exec rake db:migrate

* Database import (__requires the germany_raw.csv file in db/__)

        bundle exec rake db:import

* Run the test suite

        bundle exec rspec

* Reset Database

        bundle exec rake db:reset