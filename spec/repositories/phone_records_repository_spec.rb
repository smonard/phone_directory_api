require 'rails_helper'
require 'faker'

describe PhoneRecordsRepository do

    let!(:expected_relation) {
        double("Array[PhoneRecord]")
    }
    let!(:where_clause_relation) {
        double()
    }

    before do
        allow(where_clause_relation).to receive(:order).with(prefix: :desc).and_return(expected_relation)
    end
        
    describe "#find_by_number" do
        it "returns the matcher relation including all the specified prefixes" do
            allow(PhoneRecord).to receive(:where).with(prefix: [4897,489,48,4]).and_return(where_clause_relation)
            
            actual_matches = PhoneRecord.find_by_number([4897,489,48,4])
            
            expect(actual_matches).to be(expected_relation)
        end
    end

    describe "#find_by_name" do
        it "returns the expected record relation" do
            hint = Faker::Name.name
            allow(PhoneRecord).to receive(:where).with("phone_records.comment LIKE '#{hint}%'").and_return(where_clause_relation)
            
            actual_matches = PhoneRecord.find_by_name([hint])
            
            expect(actual_matches).to be(expected_relation)
        end

        it "returns empty relation when hint is empty" do
            
            actual_matches = PhoneRecord.find_by_name([])
            
            expect(actual_matches).to eq(PhoneRecord.none)
        end

        it "returns the matcher relation including all the specified prefixes" do
            hint = ['Frankfurt am Ma', 'Berl', 'Guay'] 
            allow(PhoneRecord).to receive(:where).with("phone_records.comment LIKE 'Frankfurt am Ma%' OR phone_records.comment LIKE 'Berl%' OR phone_records.comment LIKE 'Guay%'").and_return(where_clause_relation)
            
            actual_matches = PhoneRecord.find_by_name(hint)
            
            expect(actual_matches).to be(expected_relation)
        end
    end
end
