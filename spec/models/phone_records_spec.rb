require 'rails_helper'
require 'faker'

describe PhoneRecords do
    let(:collection) { PhoneRecords.new }

    describe "#get_by_criteria." do

        let(:expected_matches) {double("PhoneRecord Relation")}
        let(:mock) {double("Mock Relation")}

        before do
            allow(PhoneRecord).to receive(:find_by_number).and_return(mock)
            allow(PhoneRecord).to receive(:find_by_name).and_return(mock)
        end

        context 'Criteria only containing number.' do
            it "calls the repository with the specified number" do
                allow(PhoneRecord).to receive(:find_by_number).with([5]).and_return(expected_matches)
                allow(expected_matches).to receive(:or).and_return(expected_matches)

                actual_matches = collection.find_by_criteria("5")
                
                expect(actual_matches).to be(expected_matches)
            end

            it "calls the repository with all the prefixes of the specified number" do
                allow(PhoneRecord).to receive(:find_by_number).with([52415,5241,524,52,5]).and_return(expected_matches)
                allow(expected_matches).to receive(:or).and_return(expected_matches)
                
                actual_matches = collection.find_by_criteria("52415")
                
                expect(actual_matches).to be(expected_matches)
            end
        end

        context 'Criteria only containing name.' do
            it 'calls the repository for searching by name' do
                name = Faker::Name.last_name
                allow(PhoneRecord).to receive(:find_by_name).with([name]).and_return(expected_matches)
                allow(mock).to receive(:or).with(expected_matches).and_return(expected_matches)

                actual_matches = collection.find_by_criteria(name)

                expect(actual_matches).to be(expected_matches)
            end
            it 'calls the repository for searching by name when various names' do
                allow(PhoneRecord).to receive(:find_by_name).with(["Frank", "Berl"]).and_return(expected_matches)
                allow(mock).to receive(:or).with(expected_matches).and_return(expected_matches)

                actual_matches = collection.find_by_criteria("Frank Berl")

                expect(actual_matches).to be(expected_matches)
            end
        end

        context 'Criteria containing number and name.' do
            it 'calls the repository for searching by name when original criteria contains numbers too' do
                name = Faker::Name.last_name
                criteria = "#{name} #{Faker::Number.number}"
                allow(PhoneRecord).to receive(:find_by_name).with([name]).and_return(expected_matches)
                allow(mock).to receive(:or).with(expected_matches).and_return(expected_matches)

                actual_matches = collection.find_by_criteria(criteria)

                expect(actual_matches).to be(expected_matches)
            end
            it 'calls the repository for searching by number when original criteria contains name too' do
                number = Faker::Number.number
                criteria = "#{number}#{Faker::Name.name}"
                allow(PhoneRecord).to receive(:find_by_number).and_return(expected_matches)
                allow(expected_matches).to receive(:or).with(mock).and_return(expected_matches)

                actual_matches = collection.find_by_criteria(criteria)

                expect(actual_matches).to be(expected_matches)
            end
            it 'calls the repository for searching by name and number when various names and numbers' do
                allow(PhoneRecord).to receive(:find_by_number).with([895,89,8]).and_return(expected_matches)
                allow(PhoneRecord).to receive(:find_by_name).with(["Ank", "Erl"]).and_return(expected_matches)
                allow(expected_matches).to receive(:or).with(expected_matches).and_return(expected_matches)

                actual_matches = collection.find_by_criteria("Ank 895 Erl")

                expect(actual_matches).to be(expected_matches)
            end
        end
    end
end
