require 'rails_helper'

describe PhoneRecordsController do

    describe "#get/<criteria>." do
        let!(:phone_record2) {
            phone_record = PhoneRecord.create!(prefix: 4512, max_len: 14,  min_len: 10,  usage: "CD",  comment: "Berlin")
            phone_record.save
            phone_record
        }

        let!(:phone_record1) {
            phone_record = PhoneRecord.create!(prefix: 45125, max_len: 14,  min_len: 10,  usage: "CD",  comment: "Frankfurt")
            phone_record.save
            phone_record
        }

        it 'returns all data when criteria was not sent' do
            get :index
            expect(response.body).to eq([phone_record2, phone_record1].to_json)
        end

        context 'Querying with invalid characters' do

            it 'ignores invalid characters' do
                get :index, params: {criteria: "%4512%"}
                expect(response.body).to eq([phone_record2].to_json)
            end
            context 'Querying with (0)' do
                it 'ignores (0)' do
                    get :index, params: {criteria: "45(0)12"}
                    expect(response.body).to eq([phone_record2].to_json)
                end
                it 'ignores (0) with leading whitespace' do
                    get :index, params: {criteria: "45 (0)12"}
                    expect(response.body).to eq([phone_record2].to_json)
                end
                it 'ignores (0) with trailing whitespace' do
                    get :index, params: {criteria: "45(0) 12"}
                    expect(response.body).to eq([phone_record2].to_json)
                end
                it 'ignores (0) with leading and trailing whitespace' do
                    get :index, params: {criteria: "45 (0) 12"}
                    expect(response.body).to eq([phone_record2].to_json)
                end
            end

        end

        context 'Getting results by prefix number' do

            it 'returns phone records matching with the whole prefix number' do
                get :index, params: {criteria: "4512"}
                expect(response.body).to eq([phone_record2].to_json)
            end

            it 'returns phone records matching with the prefixs of the number' do
                get :index, params: {criteria: "451252141"}
                expect(response.body).to eq([phone_record1, phone_record2].to_json)
            end
            
        end

        context 'Getting results by name' do

            it 'returns phone records matching with the name' do
                get :index, params: {criteria: "Berl"}
                expect(response.body).to eq([phone_record2].to_json)
            end
            
        end

        context 'Getting results by name and prefix' do

            it 'returns phone records matching with the name and the prefix' do
                get :index, params: {criteria: "Frank 4512"}
                expect(response.body).to eq([phone_record1, phone_record2].to_json)
            end
            
        end

        context 'Getting results by name with umlauts' do

            let!(:phone_record3) {
                phone_record = PhoneRecord.create!(prefix: 99999, max_len: 14,  min_len: 10,  usage: "CD",  comment: "Wülfrath")
                phone_record.save
                phone_record
            }

            it 'returns phone records matching with the name and the prefix' do
                get :index, params: {criteria: "Wülfrath"}
                expect(response.body).to eq([phone_record3].to_json)
            end
            
        end
    end
end
